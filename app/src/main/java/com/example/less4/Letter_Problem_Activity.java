package com.example.less4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Letter_Problem_Activity extends AppCompatActivity {

    private EditText edDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.letter_problem_activity);
        init();
    }

   private void init(){
       edDescription = findViewById(R.id.edDescription);
   }

    public void onClickSubmitRequest(View view){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(1500);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
                Intent i = new Intent(Letter_Problem_Activity.this,My_Problems_Activity.class);
                startActivity(i);
            }
        }).start();
        Toast.makeText(getApplicationContext(), "Запрос отправлен", Toast.LENGTH_SHORT).show();
    }
}
