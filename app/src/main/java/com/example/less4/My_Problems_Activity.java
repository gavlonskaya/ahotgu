package com.example.less4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class My_Problems_Activity extends AppCompatActivity {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_problems_activity);
    }


    public void onClickAll(View view) {

    }

    public void onClickOpen(View view) {

    }

    public void onClickClose(View view) {

    }

    public void onClickExit(View view) {
        Intent i = new Intent(My_Problems_Activity.this, MainActivity.class);
        startActivity(i);
    }

}
