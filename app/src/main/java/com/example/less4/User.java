package com.example.less4;

public class User {
    public String id, name, surname, login, password;

    public User() {
    }

    public User(String id, String name, String surname, String login, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
    }
}