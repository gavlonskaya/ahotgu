package com.example.less4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
public class Log_In_Activity extends Activity {

    private EditText edNameRegister, edSurnameRegister, edLoginRegister, edPasswordRegister;
    private Button buttonRegister;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in_activity);
        init();
    }

    private void init() {
        edNameRegister = findViewById(R.id.edNameRegister);
        edSurnameRegister = findViewById(R.id.edSurnameRegister);
        edLoginRegister = findViewById(R.id.edLoginRegister);
        edPasswordRegister = findViewById(R.id.edPasswordRegister);
        buttonRegister = findViewById(R.id.buttonRegister);
        mAuth = FirebaseAuth.getInstance();
//        buttonRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (edNameRegister.getText().toString().isEmpty() ||
//                        edSurnameRegister.getText().toString().isEmpty() ||
//                        edLoginRegister.getText().toString().isEmpty() ||
//                        edPasswordRegister.getText().toString().isEmpty() ){
//                    Toast.makeText(Log_In_Activity.this,"Заполните все поля", Toast.LENGTH_SHORT).show();
//                }else{
//                    mAuth.signInWithEmailAndPassword(edLoginRegister.getText().toString(),edPasswordRegister.getText().toString())
//                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                                @Override
//                                public void onComplete(@NonNull Task<AuthResult> task) {
//                                    if (task.isSuccessful()){
//                                        Intent i = new Intent(Log_In_Activity.this,Entrance_Activity.class);
//                                        startActivity(i);
//                                    }else{
//                                        Toast.makeText(Log_In_Activity.this,"Возникла ошибка", Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });
//                }
//            }
//        });

    }


//    @Override
//    protected void onStart() {
//        super.onStart();
//        FirebaseUser cUser = mAuth.getCurrentUser();
//    }

    public void onClickLogIn(View view) {

        if (edNameRegister.getText().toString().isEmpty() ||
                edSurnameRegister.getText().toString().isEmpty() ||
                edLoginRegister.getText().toString().isEmpty() ||
                edPasswordRegister.getText().toString().isEmpty()) {
            Toast.makeText(Log_In_Activity.this, "Заполните все поля", Toast.LENGTH_SHORT).show();
        } else {
            mAuth.signInWithEmailAndPassword(edLoginRegister.getText().toString(), edPasswordRegister.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Intent i = new Intent(Log_In_Activity.this, Entrance_Activity.class);
                                startActivity(i);
                            } else {
                                Toast.makeText(Log_In_Activity.this, "Возникла ошибка", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }
}

